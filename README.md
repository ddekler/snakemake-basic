# Snakemake example

A quick example to showcase [snakemake]().

## Installation

Install snakemake in your home directory:
```bash
$ pip install snakemake pulp==2.3.0
```

## Get the code

```bash
git clone https://git.ecdf.ed.ac.uk/ddekler/snakemake-basic
cd snakemake-basic
```

## Running snakemake

The following command will instruct snakemake to run a dummy analysis for 9 events:

```bash
$ snakemake --cores 2
```

There should be a directory called `data` with 9 files `output_x.rds`.
