

## Create a list, 1 - 9, that represest event IDs.
event_ids = list(range(1, 10))

## The first rule will analyse all events in the list.
rule analyse_all_events:
    input: 
        expand("data/output_{event_id}.rds", zip, event_id=event_ids)

## A helper rule to create the 'data' directory.
rule create_data_dir:
    input: "data"
    output: directory("data")
    shell: "mkdir data"

## A rule to analyse one event for a given ID.
rule analyse_event:
    output: "data/output_{event_id}.rds"
    shell: "Rscript test.R {wildcards.event_id}"
